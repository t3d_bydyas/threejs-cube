import * as THREE from 'three'
import { OrbitControls } from "three/addons/controls/OrbitControls"
import gsap from "gsap";

const coordsBeforeExplosion = []

const getRandomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)

// DOM
const canvas = document.querySelector('#webgl')
const btnCreate = document.querySelector('#create')
const btnExplode = document.querySelector('#explode')
const btnRebuild = document.querySelector('#re-build')

btnCreate.onclick = () => {
    // Getting sizes from user
    const width = document.querySelector('input[name="width"]').value
    const height = document.querySelector('input[name="height"]').value
    const depth = document.querySelector('input[name="depth"]').value

    if (width && height && depth) {
        // Manipulating with UI
        document.querySelector('.inputs').style.display= "none"
        btnCreate.style.display = 'none'
        btnExplode.style.display = 'block'

        // Creating objects
        createCube(width, height, depth)

        // Updating camera
        camera.position.set(width/2, height/2, depth/2*5)
    }
}

btnExplode.onclick = () => {
    // Manipulating with UI
    btnExplode.style.display = 'none'
    btnRebuild.style.display = 'block'

    // Spreading the meshes around 
    if (meshes.length) {
        meshes.forEach(mesh => {
            // Save initial coords
            coordsBeforeExplosion.push([
                mesh.position.x,
                mesh.position.y,
                mesh.position.z
            ])
            // Move mesh
            gsap.to(mesh.position, { 
                duration: 0.3, 
                x: (Math.random() * Math.PI/2) * getRandomNumber(-17, 17), 
                y: (Math.random() * Math.PI/2) * getRandomNumber(-13, 13), 
                z: (Math.random() * Math.PI/2) * getRandomNumber(-15, 15) 
            })
        })
    }
}

btnRebuild.onclick = () => {
    // Manipulating with UI
    btnRebuild.style.display = 'none'
    btnExplode.style.display = 'block'

    // Moving back the meshes
    for (let i = 0; i < meshes.length; i++) {
        gsap.to(meshes[i].position, { 
            duration: 0.3,
            x: coordsBeforeExplosion[i][0], 
            y: coordsBeforeExplosion[i][1],
            z: coordsBeforeExplosion[i][2] 
        })
    }
}


// Textures
const textureLoader = new THREE.TextureLoader()
const lavaTexture = textureLoader.load('/textures/Lava_001/Lava_001_COLOR.png')
const blueIceTexture = textureLoader.load('/textures/Blue_Ice_001/Blue_Ice_001_COLOR.jpg')
const groundDirtTexture = textureLoader.load('/textures/Ground_Dirt_007/Ground_Dirt_007_basecolor.jpg')


// Sizing
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight
}

window.addEventListener('resize', () => {
  // Updating sizes
  sizes.width = window.innerWidth,
  sizes.height = window.innerHeight

  // Updating camera
  camera.aspect = sizes.width / sizes.height
  camera.updateProjectionMatrix()

  // Updating renderer
  renderer.setSize(sizes.width, sizes.height)
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})


// Scene
const scene = new THREE.Scene()


// Helpers
const axesHelper = new THREE.AxesHelper()
scene.add(axesHelper)


// Geometries
const boxGeometry = new THREE.BoxGeometry(1, 1, 1)
const sphereGeometry = new THREE.SphereGeometry(0.5, 16, 8)
const cylinderGeometry = new THREE.CylinderGeometry( 0.5, 0.5, 1, 16 )
const geometries = [boxGeometry, sphereGeometry, cylinderGeometry]


// Materials
const lavaMaterial = new THREE.MeshBasicMaterial({ map: lavaTexture})
const blueIceMaterial = new THREE.MeshBasicMaterial({ map: blueIceTexture})
const groundDirtMaterial = new THREE.MeshBasicMaterial({ map: groundDirtTexture})
const materials = [lavaMaterial, blueIceMaterial, groundDirtMaterial]


// Objects
const meshes = []
const cube = new THREE.Group()
scene.add(cube)

function createCube(width=0, height=0, depth=0) {   
    for (let i = 0; i < width; i++) {
        for (let j = 0; j < height; j++) {
            for (let k = 0; k < depth; k++) {
                // Getting a random number as index for our geometry/material arrays
                const randomGeoIndex = Math.floor(Math.random() * geometries.length)
                const randomMatIndex = Math.floor(Math.random() * geometries.length)

                const mesh = new THREE.Mesh(geometries[randomGeoIndex], materials[randomMatIndex])
                mesh.position.x = i
                mesh.position.y = j
                mesh.position.z = k                
                cube.add(mesh)

                meshes.push(mesh)
            }
        }
    }    
    cube.translateX(1/2 - width/2)
    cube.translateY(1/2 - height/2)
    cube.translateZ(1/2 - depth/2)
}


// Camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.set(0, 0.5, 2)
scene.add(camera)


// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true


// Renderer
const renderer = new THREE.WebGLRenderer({ canvas })
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))


const loop = () => {
    window.requestAnimationFrame(loop)

    camera.lookAt(cube.position)

    controls.update()

    renderer.render(scene, camera)
}

loop()